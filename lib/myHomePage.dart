
import 'dart:io';

import 'package:fileuploader/storageService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();

}
class _MyHomePageState extends State<MyHomePage> {
 final Storage storage = Storage();
  File imageFile;
  File _compressedFile;
  bool visible =false;
  String fileName;

  Widget _showImageDialog() {
    showDialog(context: context, builder: (context) {
      return AlertDialog(
        title: Text("please choose app option "),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              onTap: () {
                getFromCamera();
              },
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.all(10.0),
                    child: Icon(
                      Icons.camera,
                      color: Colors.red,
                    ),
                  ),
                  Text("camera",
                    style: TextStyle(color: Colors.deepPurple),)
                ],
              ),
            ),
            InkWell(
              onTap: () {
                getFromGallery();
              },
              child: Row(
                children: [
                  Padding(padding: EdgeInsets.all(10.0),
                    child: Icon(
                      Icons.browse_gallery_rounded,
                      color: Colors.red,
                    ),
                  ),
                  Text("gallery",
                    style: TextStyle(color: Colors.deepPurple),)
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

   compress() async {
    var result = await FlutterImageCompress.compressAndGetFile(
      imageFile.absolute.path,
      imageFile.path + 'compressed.jpg',
      quality: 66,
    );
    setState(() {
      _compressedFile = result;
    });
  }
   getFromGallery() async {
    var pickedFile = await ImagePicker.pickImage(
      source: ImageSource.gallery,
      maxHeight: 1080,
      maxWidth: 1080,
    );
    await _cropImage(pickedFile.path);
    await compress();
    Navigator.pop(context);
  }

  getFromCamera() async {
    var pickedFile = await ImagePicker.pickImage(
      source: ImageSource.camera,
      maxHeight: 1080,
      maxWidth: 1080,
    );

    await _cropImage(pickedFile.path);
    await compress();
    Navigator.pop(context);
  }

  _cropImage(filePath) async {
    File croppedImage = await ImageCropper().cropImage(
        sourcePath: filePath,
        maxWidth: 1080,
        maxHeight: 1080,
        aspectRatio: CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
        androidUiSettings: androidUiSettings(),
        compressQuality: 70
    );
    final path = croppedImage.path.toString();
    print(path);


    storage.upload(path, _compressedFile).then((value) =>
        showDialog(context: context, builder: (BuildContext context)=>_buildPopupDialog(context)));

    if (croppedImage != null) {
      imageFile = croppedImage;
    }
    setState(() {
      visible=true;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Visibility(
              visible: false,
              child: Text('Before compressing image size is'),
            ),
            if (imageFile != null) Text('${imageFile.lengthSync()} bytes'),
            const Divider(),
            Visibility(
              visible: false,
              child: Text('Before compressing image size is'),
            ),
            if (_compressedFile != null)
              Image.file(
                _compressedFile,
                height: 200,
                width: 200,
              ),
            if (_compressedFile != null)
              Text('${_compressedFile.lengthSync()} bytes'),
            const Divider(),
            ElevatedButton(
              onPressed: () async {
                await _showImageDialog();
              },
              child: const Text('Upload Image'),
            ),
          ],
        ),
      ),
    );
  }
  static AndroidUiSettings androidUiSettings()=>AndroidUiSettings(lockAspectRatio: false,
  );

}

Widget _buildPopupDialog(BuildContext context) {
  return AlertDialog(

    content: Column(
      mainAxisSize: MainAxisSize.min,

      children: <Widget>[
        Container(
            margin: EdgeInsets.all(10.0),
            child: Text("Image upload successfully😀",style: TextStyle(color: Colors.brown[900],fontSize: 24,fontWeight: FontWeight.w500),)
        ),
      ],
    ),
    actions: <Widget>[
      Center(
        child: ElevatedButton(
          onPressed: () async {
            Navigator.of(context).pop();

          },
          child: const Text('close'),
        ),
      ),
    ],
  );
}


