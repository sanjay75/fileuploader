import 'package:fileuploader/myHomePage.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:firebase_core/firebase_core.dart' as firebase_core;
import 'dart:io';
class Storage {
  final firebase_storage.FirebaseStorage storage = firebase_storage.FirebaseStorage.instance;


  Future<void> upload (String filepath,filename) async {
    File file = File(filepath);
    try{
      await storage.ref('home/$filename').putFile(file);

    } on firebase_core.FirebaseException catch(error) {
      print(error);

    }



  }
}